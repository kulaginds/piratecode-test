<?php

namespace FileTransfer;

interface IConnection {
	public function connect($hostname, $username, $password, $port);

	public function cd($dir);

	public function download($from, $to = NULL);

	public function close();

	public function pwd();

	public function upload($from, $to = NULL);

	public function exec($cmd);

	public function getDefaultPort();
}

class Factory {

	protected $connections;

	private $upload_path;

	public function __construct($upload_path = NULL) {
		// конфигурация подключений в формате:
		//	ключ - тип протокола
		//	аргумент 0 - расположение файла класса
		//	аргумент 1 - адрес класса в пространстве имен PHP
		$this->connections = array(
			'ftp' => array(
				__DIR__.'/Connections/FTPConnection.php',
				'\FileTransfer\Connections\FTPConnection',
			),
			'ssh' => array(
				__DIR__.'/Connections/SSHConnection.php',
				'\FileTransfer\Connections\SSHConnection',
			),
		);

		// локальная папка, в которую будут загружаться файлы
		// и из которой будут закачиваться файлы
		$this->upload_path = empty($upload_path)?dirname($_SERVER['SCRIPT_FILENAME']):$upload_path;
	}

	public function getConnection($type, $username, $password, $hostname, $port = null) {
		$type = strtolower($type);

		if (!array_key_exists($type, $this->connections))
			throw new \Exception("Unknown connection type: $type.");

		$conn_type = $this->connections[$type];

		if (!is_file($conn_type[0]))
			throw new \Exception("Connection $type class not found!");
		
		include_once $conn_type[0];

		if (!class_exists($conn_type[1]))
			throw new \Exception("Class $conn_type[1] not exists!");

		$connection = new $conn_type[1]($this->upload_path);

		if (!($connection instanceof IConnection))
			throw new \Exception("Class $conn_type[1] not realized interface IConnection!");

		if (empty($hostname))
			throw new \Exception('Hostname should not be empty!');

		if (empty($username))
			throw new \Exception('Username should not be empty!');

		if (empty($password))
			throw new \Exception('Password should not be empty!');

		if (!empty($port) && !ctype_digit($port))
			throw new \Exception('Port must be digit!');

		if ($port < 0 || $port > 65536)
			throw new \Exception('Port must be in limit [0, 65536]!');

		$port = empty($port)?$connection->getDefaultPort():$port;

		$connection->connect($hostname, $username, $password, $port);

		return $connection;
	}
}

