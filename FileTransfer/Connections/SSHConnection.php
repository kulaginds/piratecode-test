<?php

namespace FileTransfer\Connections;

class SSHConnection implements \FileTransfer\IConnection {

	const PORT = 22; // порт протокола по умолчанию

	private $current_path; // локальная переменная pwd
	/*
		Решение хранить путь удаленного сервера было принято
		после тестирования ситуаций перехода в директорию и
		чтения списка файлов. При установке любой папки, список
		файлов оставался прежним - файлы в корне удаленного
		пользователя.
	*/

	private $conn_id; // идентификатор подключения

	private $upload_path; // локальная папка файлов

	public function __construct($upload_path) {
		if (FALSE == is_writable($upload_path))
			throw new \Exception("Upload directory not writeable: $upload_path.");

		$this->upload_path = $upload_path;

		// функции ssh2_* не будут работать без php_ssh2
		if (!function_exists('ssh2_connect'))
			throw new \Exception("Requied php_ssh2 module!");
	}

	public function connect($hostname, $username, $password, $port) {
		$this->conn_id = @ssh2_connect($hostname, $port);

		if (FALSE == $this->conn_id)
			throw new \Exception("Can't connect to $hostname");

		if (FALSE == @ssh2_auth_password($this->conn_id, $username, $password))
			throw new \Exception("Can't login to $username@$hostname using password");
	}

	public function cd($dir) {
		/*if (FALSE == ssh2_exec($this->conn_id, "cd $dir"))
			throw new \Exception("Can't change directory on: $dir.");*/

		$this->current_path = $dir.'/';

		return $this;
	}

	public function download($from, $to = NULL) {
		if (empty($to))
			$to = $this->upload_path.'/'.basename($from);

		if (FALSE == ssh2_scp_recv($this->conn_id, $this->current_path.$from, $to))
			throw new \Exception("Can't download file $from.");

		return $this;
	}

	public function close() {
		$this->exec('exit');
	}

	public function pwd() {
		return $this->exec("cd $this->current_path && pwd");
	}

	public function upload($from, $to = NULL) {
		$from = $this->upload_path.'/'.$from;

		if (empty($to))
			$to = basename($from);

		$to = $this->current_path.$to;

		if (!is_file($from))
			throw new \Exception("Can't find input file: $from.");

		if (FALSE == ssh2_scp_send($this->conn_id, $from, $to))
			throw new \Exception("Can't upload file $from.");

		return $this;
	}

	public function exec($cmd) {
		$stream = ssh2_exec($this->conn_id, "cd $this->current_path && $cmd");
		
		stream_set_blocking($stream, true);

		if (FALSE == $stream)
			throw new \Exception("Can't execute command $cmd.");

		$result = ssh2_fetch_stream($stream, SSH2_STREAM_STDIO);

		return stream_get_contents($result);
	}

	public function getDefaultPort() {
		return self::PORT;
	}

}

