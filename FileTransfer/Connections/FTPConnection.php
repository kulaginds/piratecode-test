<?php

namespace FileTransfer\Connections;

class FTPConnection implements \FileTransfer\IConnection {

	const PORT = 21; // порт протокола по умолчанию

	private $conn_id; // идентификатор подключения

	private $upload_path; // локальная папка файлов

	public function __construct($upload_path) {
		if (FALSE == is_writable($upload_path))
			throw new \Exception("Upload directory not writeable: $upload_path.");

		$this->upload_path = $upload_path;
	}

	public function connect($hostname, $username, $password, $port) {
		$this->conn_id = ftp_connect($hostname, $port);

		if (FALSE == $this->conn_id)
			throw new \Exception("Can't connect to $hostname");

		if (FALSE == @ftp_login($this->conn_id, $username, $password))
			throw new \Exception("Can't login to $username@$hostname using password");
	}

	public function cd($dir) {
		if (FALSE == ftp_chdir($this->conn_id, $dir))
			throw new \Exception("Can't change directory on: $dir.");

		return $this;
	}

	public function download($from, $to = NULL) {
		if (empty($to))
			$to = $this->upload_path.'/'.basename($from);

		if (FALSE == ftp_get($this->conn_id, $to, $from, FTP_BINARY))
			throw new \Exception("Can't download file $from.");

		return $this;
	}

	public function close() {
		if (FALSE == ftp_close($this->conn_id))
			throw new \Exception('Can\'t close connection!');
	}

	public function pwd() {
		$dir = ftp_pwd($this->conn_id);
		
		if (FALSE == $dir)
			throw new \Exception('Can\'t get current path.');

		return $dir;
	}

	public function upload($from, $to = NULL) {
		$from = $this->upload_path.'/'.$from;
		
		if (empty($to))
			$to = basename($from);

		if (!is_file($from))
			throw new \Exception("Can't find input file: $from.");

		if (FALSE == ftp_put($this->conn_id, $to, $from, FTP_BINARY))
			throw new \Exception("Can't upload file $from.");

		return $this;
	}

	public function exec($cmd) {
		$result = ftp_raw($this->conn_id, $cmd);

		return implode($result, "\n");
	}

	public function getDefaultPort() {
		return self::PORT;
	}

}

